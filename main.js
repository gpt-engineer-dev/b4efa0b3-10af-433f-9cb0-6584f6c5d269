// Retrieve tasks from local storage or initialize an empty array
let tasks = JSON.parse(localStorage.getItem("tasks")) || [];

// Function to render the tasks on the page
function renderTasks() {
  const taskList = document.getElementById("taskList");
  taskList.innerHTML = "";

  tasks.forEach((task, index) => {
    const listItem = document.createElement("li");
    listItem.className =
      "flex items-center justify-between bg-white rounded-lg p-4 mb-2 shadow";
    if (task.completed) {
      listItem.classList.add("completed");
    }

    const taskText = document.createElement("span");
    taskText.textContent = task.text;

    const deleteBtn = document.createElement("button");
    deleteBtn.className =
      "px-2 py-1 rounded bg-red-500 hover:bg-red-600 text-white";
    deleteBtn.innerHTML = '<i class="fas fa-trash"></i>';
    deleteBtn.addEventListener("click", () => {
      deleteTask(index);
    });

    const completeBtn = document.createElement("button");
    completeBtn.className =
      "px-2 py-1 rounded bg-red-500 hover:bg-red-600 text-white";
    completeBtn.innerHTML = '<i class="fas fa-check"></i>';
    completeBtn.addEventListener("click", () => {
      toggleComplete(index);
    });

    listItem.appendChild(taskText);
    listItem.appendChild(deleteBtn);
    listItem.appendChild(completeBtn);

    taskList.appendChild(listItem);
  });
}

// Function to add a new task
function addTask() {
  const taskInput = document.getElementById("taskInput");
  const taskText = taskInput.value.trim();

  if (taskText !== "") {
    const newTask = {
      text: taskText,
      completed: false,
    };

    tasks.push(newTask);
    renderTasks();
    saveTasksToLocalStorage();

    taskInput.value = "";
  }
}

// Function to delete a task
function deleteTask(index) {
  tasks.splice(index, 1);
  renderTasks();
  saveTasksToLocalStorage();
}

// Function to toggle the completion status of a task
function toggleComplete(index) {
  tasks[index].completed = !tasks[index].completed;
  renderTasks();
  saveTasksToLocalStorage();
}

// Function to save tasks to local storage
function saveTasksToLocalStorage() {
  localStorage.setItem("tasks", JSON.stringify(tasks));
}

// Event listener for adding a task
const addTaskBtn = document.getElementById("addTaskBtn");
addTaskBtn.addEventListener("click", addTask);

// Initial rendering of tasks
renderTasks();
